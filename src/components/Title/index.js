import React, { Component } from 'react';
import './style.scss';

class Title extends Component {
    constructor(props) {
        super(props);
        this.state = this.getState();
    }

    getState() {
        let name = this.props.name.split(' ');
        let rightName = name[1];
        for (let index = 2; index < name.length; index++) {
            rightName += `${name[index]} `;
        }

        let state = {
            leftName: name[0],
            rightName: rightName,
        }

        if (this.props.type === 0) {
            state['shapeStyle'] = `c-title-art__rectangle`;
        } else if (this.props.type === 1) {
            state['shapeStyle'] = `c-title-art__cross`;
        } else if (this.props.type === 2) {
            state['shapeStyle'] = `c-title-art__circle`;
        }

        return state;
    }

    render() {
        return (
            <div id='l-title' className='l-flex--row-nowrap l-flex--center-center'>
                <div id='l-title__name'>
                    <label id='c-title__left-name'>
                        {this.state.leftName}
                    </label> <label id='c-title__right-name'>
                        {this.state.rightName}
                    </label>
                </div>
                <div id='l-title__title-art' className='l-flex--row-nowrap l-flex--center-center'>
                    <div id='c-title-art__line'></div>
                    <div id={this.state.shapeStyle} className='l-title-art__shape-config'></div>
                </div>
            </div>
        );
    }
}

export default Title;