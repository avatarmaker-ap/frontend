import React from 'react';
import './style.scss';

const Circle = () => {
    return (
        <div id='c-circle-art__container'>
            <div id='c-circle-art--0'></div>
            <div id='c-circle-art--1'></div>
        </div>
    );
}

export default Circle;