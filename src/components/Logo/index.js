import React, { Component } from 'react';
import './style.scss';

class Logo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            logoPos: this.props.config.pos,
            logoSize: this.props.config.size,
        } 
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.config.pos !== nextProps.config.pos) {
            this.setState({
                logoPos: nextProps.config.pos,
            })
        }

        if (this.props.config.size !== nextProps.config.size) {
            this.setState({
                logoSize: nextProps.config.size,
            });
        }
    }

    render() {
        return (
            <div id='l-logo' className='l-flex--row-nowrap h-animation-config' data-position={this.state.logoPos} data-size={this.state.logoSize}>
                <div className='l-flex--column-nowrap l-flex--center-center'>
                    <img id='c-logo__avata' src='/icons/avata.svg' alt='avata'></img>
                    <img id='c-logo__make' src='/icons/make.svg' alt='make'></img>
                </div>
                <img id='c-logo__r' src='/icons/r.svg' alt='r'></img>
            </div>
        );
    }
}

export default Logo;