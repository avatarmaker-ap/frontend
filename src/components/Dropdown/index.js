import React, { Component } from 'react';
import './style.scss';

class Dropdown extends Component {
    constructor(props) {
        super(props);
    }

    showOptionList() {
        let optionList = [];
        for (let row=0; row < this.props.list.length; row++) {
            const option = this.props.list[row];
            optionList.push(
                <option key={option.value} value={option.value}>{option.name}</option>
            );
        }
        return optionList;
    }

    render() {
        return (
            <div id='l-select' className='l-flex--row-nowrap l-flex--center-center' data-color={this.props.color}>
                <img src={this.props.src} id='c-select__icon' alt='dropdown-icon' />
                <div id='l-select__field'>
                    <select id='c-select__field' onChange={(e) => this.props.sendValue(e.target.value)}>
                        {this.showOptionList()}
                    </select>                               
                    <div id='l-input__art' className='l-flex--row-nowrap l-flex--center-center'>
                        <div id='c-input__line-art'></div>
                        <div id='c-input__triangle-art' className='h-animation-config' ></div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Dropdown;