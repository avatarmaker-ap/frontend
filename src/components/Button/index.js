import React, { Component } from 'react';
import './style.scss';

class Button extends Component {
    constructor(props) {
        super(props);
        this.state = {
            buttonState: `idle`,
        };
        this.mouseEnter = this.mouseEnter.bind(this);
        this.mouseLeave = this.mouseLeave.bind(this);
    }

    mouseEnter() {
        this.setState({
            buttonState: `mouse-enter`,
        });
    }

    mouseLeave() {
        this.setState({
            buttonState: `mouse-leave`,
        });
    }

    render() {
        return (
            <button id='c-button' data-state={this.state.buttonState} onMouseEnter={this.mouseEnter} onMouseLeave={this.mouseLeave} onClick={this.props.do}>
                <div id='c-rectangle-art--0' ></div>
                <div id='c-rectangle-art--1' ></div>
                <div id='c-rectangle-art--2' ></div>
                <div id='c-rectangle-art--3' ></div>
                <div id='c-rectangle-art--4' ></div>
                <div id='c-rectangle-art--5' ></div>
                <div id='c-button__background'></div>
                <label className='l-flex--row-nowrap l-flex--center-center'>
                    {this.props.name}
                </label>
            </button>
        );
    }
}

export default Button;