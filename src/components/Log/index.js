import React, {Component} from 'react';
import './style.scss';

class Log extends Component {
    constructor(props) {
        super(props);
        this.messages = [];
        this.state = {
            display: false,
            logs: "",
        }
    }

    componentWillReceiveProps(nextProps) {
        let messageExists = this.messages.includes(nextProps.message);
        if (nextProps.operation === 'ADD' && !messageExists) {
            this.messages.push(nextProps.message);
            this.setLogs();
        } else if (nextProps.operation === 'DEL' && messageExists) {
            let index = this.messages.indexOf(nextProps.message);
            if (index !== -1) this.messages.splice(index, 1);
            this.setLogs();
        }
    }

    setLogs() {
        let logs = '';
        let display = false;
        if (this.messages.length !== 0) {
            logs = this.messages[0];
            display = true;
            for (let index = 1; index < this.messages.length; index++) {
                logs += ', ' + this.messages[index];
            }
        }

        this.setState({
            display,
            logs,
        })
    }

    render() {
        return (
            <div className={`l-log ${this.props.className} l-flex--row-nowrap l-flex--center-center`} data-display={this.state.display}>
                <p>
                    {this.state.logs}
                </p>
                <div className='c-log__loading'>
                </div>
            </div>
        );
    }
}

export default Log;