import React, { Component } from 'react';
import './style.scss';

class Background extends Component {
    constructor(props) {
        super(props);
        this.state = {
            displayState: Background.getDisplayState(this.props.display),
            backgroundColor: Background.getBackground(this.props.type),
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.display !== nextProps.display){
            this.setState({
                displayState: Background.getDisplayState(nextProps.display),
            });
        }
        if (this.props.type !== nextProps.type) {
            this.setState({
                backgroundColor: Background.getBackground(nextProps.type),
            });
        }
    }

    static getDisplayState(display) {
        if (display) {
            return 'idle';
        } else {
            return 'fly-away';
        }
    }

    static getBackground(type) {
        if (type === 'LOADING') return 'red';
        else if (type === 'MAIN') return 'blue';
    }

    render() {
        return (
            <div id='l-background' className={`l-section h-animation-config ${this.props.className}`} data-color={this.state.backgroundColor} data-display={this.state.displayState}>
            </div>
        );
    }
}

export default Background;