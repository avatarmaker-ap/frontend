import React, { Component } from 'react';

import Background from 'components/Background';
import './style.scss'

class Button extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isActive: this.props.isActive,
            title: this.props.title,
            message: this.props.message,
        };

        this.closePopup = this.closePopup.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props !== nextProps) {
            this.setState({
                isActive: nextProps.isActive,
                title: nextProps.title,
                message: nextProps.message,
            });
        }
    }

    closePopup(e) {
        this.props.setPopup(false, "", "");
    }

    render() {
        return (
            <div id='l-popup' data-display={this.state.isActive}>
                <Background type={'MAIN'} display={true} className='c-popup__background'/>
                <div id='l-popup__content' className='h-animation-config l-flex--row-nowrap l-flex--center-center' >
                    <img id='c-popup__close-icon' alt='close' src='/icons/close.svg' onClick={this.closePopup} />
                    <div id='l-popup__data'>
                        <h1 id='c-popup__title'>
                            {this.state.title}
                        </h1>
                        <p id='c-popup__message'>
                            {this.state.message}
                        </p>
                    </div>
                </div>
            </div>
        );
    }
}

export default Button;