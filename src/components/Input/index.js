import React, { Component } from 'react';
import './style.scss';

class Input extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div id='l-input'className='l-flex--row-nowrap l-flex--center-center' data-color={this.props.color}>
                <img src={this.props.src} id='c-input__icon' alt='input-icon'/>
                <div id='l-input__field'>
                    <input id='c-input__field' type='text' placeholder={this.props.placeholder} onKeyUp={(e) => this.props.sendValue(e.target.value)} />                                
                    <div id='l-input__art' className='l-flex--row-nowrap l-flex--center-center'>
                        <div id='c-input__line-art'></div>
                        <div id='c-input__triangle-art' className='h-animation-config' ></div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Input;