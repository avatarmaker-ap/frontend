import React, { Component } from 'react';
import deepcopy from 'deepcopy';
import './style.scss';

import Button from 'components/Button';
import Title from 'components/Title';
import Input from 'components/Input';
import Log from 'components/Log';
import Avatar from './components/Avatar';
import ItemMenu from './components/ItemMenu';


const initItems = [
    {
        id: 0,
        url: undefined,
    },
    {
        id: 0,
        url: undefined,
        // url: "/imgs/items/clothes-part.jpg",
    },
    {
        id: 0,
        url: undefined,
        // url: "/imgs/items/pants-part.jpg",
    },
    {
        id: 0,
        url: undefined,
        // url: "/imgs/items/shoes-part.jpg",
    }
]

class Customize extends Component {
    constructor(props) {
        super(props);

        let titleType, character, displayDirection, displayStart;

        if (this.props.isMale) {
            titleType = 1;
            displayDirection = 'forward';
            displayStart = 'left';
        } else {
            titleType = 2;
            displayDirection = 'backward';
            displayStart = 'right';
        }

        this.state = {
            displayState: this.props.display ? 'appear' : 'hide',
            titleType,
            character,
            displayDirection,
            displayStart,
            characterItems: {
                hair: deepcopy(initItems[0]),
                clothes: deepcopy(initItems[0]),
                pants: deepcopy(initItems[0]),
                shoes: deepcopy(initItems[0]),
            },
            code: "",
            log: {
                operation: undefined,
                message: undefined,
            }
        };

        this.setCharacterItem = this.setCharacterItem.bind(this);
        this.save = this.save.bind(this);
    }

    linkRef = React.createRef();

    fetchItems() {
        fetch(`https://avatarmaker-item.herokuapp.com/items/${this.props.isMale? 'm': 'f'}`, {
            method: 'GET',
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
        }).then(response => {
            return response.json()
        }).then(result => {
            this.setState({
                items: result.concat(deepcopy(initItems)),
            })
        }).catch(err => {
            console.log(err);
        });
    } 

    setCharacterItem(type, id, url) {
        let characterItems = this.state.characterItems;
        characterItems[type].id = id;
        characterItems[type].url = url;
        this.setState({
            characterItems
        });
    }

    getItemBy(id) {
        return this.state.items.filter((item) => item.id === id)[0];
    }

    backToGenderOption() {
        this.props.setScene(0);
    }

    save(type) {
        this.setState({
            log: {
                operation: 'ADD',
                message: 'creating image',
            }
        });

        switch(type) {
            case 'png': {
                let data = []
                if (this.props.isMale) {
                    data.push("https://drive.google.com/uc?id=1VBqcoF-KCPzWqagOR-H_xGCGmJAy0Iiz");
                } else {
                    data.push("https://drive.google.com/uc?id=153TLDSx989l6l12YANRcTZAJUYiG_42Y");
                }
        
                data = data.concat([
                    this.state.characterItems['hair'].url,
                    this.state.characterItems['pants'].url,
                    this.state.characterItems['shoes'].url,
                    this.state.characterItems['clothes'].url,
                ])
                fetch('https://avatarmaker-image.herokuapp.com/download', {
                    method: 'POST',
                    body: JSON.stringify(data),
                    headers: {
                        'Content-Type': 'application/json',
                    },
                }).then(response => {
                    return response.blob();
                }).then(blob => {
                    const href = window.URL.createObjectURL(blob);
                    const a = this.linkRef.current;
                    a.download = 'avatar.png';
                    a.href = href;
                    a.click();
                    a.href = '';
                    this.setState({
                        log: {
                            operation: 'DEL',
                            message: 'creating image',
                        }
                    });
                }).catch(err => {
                    this.setState({
                        log: {
                            operation: 'DEL',
                            message: 'creating image',
                        }
                    });
                    this.props.showMessage("error", "server can't create image");
                }); 
                break;
            }
        }
    }

    generateCode() {
        this.setState({
            log: {
                operation: 'ADD',
                message: 'generating code'
            }
        })
        const data = {
            "data": Object.keys(this.state.characterItems).map((key) => this.state.characterItems[key].id),
        };

        fetch('https://avatarmaker-code.herokuapp.com/convert/encode', {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }).then(response => {
            return response.json();
        }).then(result => {
            if (result.status === 200) {
                this.setState({
                    log: {
                        operation: 'DEL',
                        message: 'generating code'
                    }
                })
                this.props.showMessage("the code", result.data); 
            }
        }).catch(err => {
            this.setState({
                log: {
                    operation: 'DEL',
                    message: 'generating code'
                }
            })
            this.props.showMessage("error", "failed to generate code :(");
        }); 
    }

    loadCode() {
        this.setState({
            log: {
                operation: 'ADD',
                message: 'loading code',
            }
        })
        fetch(`https://avatarmaker-code.herokuapp.com/convert/decode?code=${this.state.code}`, {
            method: 'GET'
        })
        .then(response => {
            return response.json();
        }).then((result) => {
            const characterItems = this.state.characterItems;
            if (result.status = 200) {
                for (let item of Object.keys(characterItems)) {
                    const id = result.data.shift();
                    characterItems[item] = {
                        id,
                        url: this.getItemBy(id).url,
                    };
                }

                this.setState({
                    log: {
                        operation: 'DEL',
                        message: 'loading code',
                    },
                    characterItems,
                });
            }   
        }).catch(err => {
            this.setState({
                log: {
                    operation: 'DEL',
                    message: 'loading code',
                },
            });
            this.props.showMessage("error", "failed to load code");
        }); 
    }

    componentDidMount() {
        this.fetchItems();
    }
    
    render() {
        if (this.state.items === undefined) {
            this.props.setLoading(true, 'fetching items...');
            return <div></div>
        } else {
            this.props.setLoading(false, '');
            return (
                <section id='l-customize' className='l-section l-flex--column-nowrap l-flex--center-center' data-display={this.state.displayState} >
                    <div id='l-customize__nav' className='l-flex--row-nowrap l-flex--center-center'>
                        <Log operation={this.state.log.operation} className='l-nav__logger' message={this.state.log.message}/>
                        <Button name='back' do={this.backToGenderOption.bind(this)} />
                    </div>
                    <Title name={'character custom'} type={this.state.titleType} />
                    <div id='l-customize__content' className='l-flex--row-nowrap l-flex--center-center' data-direction={this.state.displayDirection}>
                        <div id='l-customize__character' data-direction={this.state.displayStart}>
                            <Avatar display={true} isMale={this.props.isMale} items={this.state.characterItems} />
                        </div>
                        <div id='l-customize__tools' className='l-flex--column-nowrap l-flex--center-center' data-start={this.state.displayStart}>
                            <div className='l-customize__tool'>
                                <ItemMenu display={true} isMale={this.props.isMale} items={this.state.items} setCharacterItem={(type, id, url) => this.setCharacterItem(type, id, url)}/>
                            </div>
                            <div id='l-customize__save-tool' className='l-flex--row-nowrap l-flex--center-center'>
                                <div className='l-customize__tool'>
                                    <Button name='save as png' do={() => this.save('png')} />
                                </div>
                                <div className='l-customize__tool'>
                                    <Button name='generate code' do={this.generateCode.bind(this)} />
                                </div>
                            </div>
                            <div id='l-customize__code-loader' className='l-flex--row-nowrap l-flex--center-center'>
                                <div className='l-customize__tool'>
                                    <Input src='/icons/qr-code.svg' sendValue={(code) => this.setState({code})} type={0} placeholder='character code' color='white' />
                                </div>
                                <div className='l-customize__tool'>
                                    <Button name='load' do={this.loadCode.bind(this)} />
                                </div>
                            </div>
                        </div>
                    </div>
                    <a ref={this.linkRef}/>
                </section>
            );
        }
    }
}

export default Customize;