import React, { Component } from 'react';
import './style.scss';

import Dropdown from 'components/Dropdown';
import Item from './components/Item';

const MAX_ROW = 2;
const MAX_COL = 6;
const CATEGORY_INDEX = {
    'hair': 0,
    'clothes': 1,
    'pants': 2,
    'shoes': 3,
}

class ItemMenu extends Component {
    constructor(props) {
        super(props);
        this.animateContent = this.animateContent.bind(this);
        this.categorize = this.categorize.bind(this);
        this.sort = this.sort.bind(this);
        this.changePage = this.changePage.bind(this);

        const itemByTypes = {
            'hair': [],
            'clothes': [],
            'pants': [],
            'shoes': [],
        }
        
        for (let item of props.items) {
            if (item.type) {
                itemByTypes[item.type].push(item);
            }
        }


        this.state = {
            displayState: this.props.display? 'appear': 'hide',
            menuButtonStart: this.props.isMale? 'left': 'right',
            menuButtonIsClicked: true,
            menuButtonState: 'clicked',
            orderStatus: 1,
            page: 0,
            itemByTypes,
            categoryList: [
                {
                    name: 'hair', 
                    value: 'hair',
                },
                {
                    name: 'clothes', 
                    value: 'clothes',
                },
                {
                    name: 'pants', 
                    value: 'pants',
                },
                {
                    name: 'shoes', 
                    value: 'shoes',
                },
            ],
            sorterList: [
                {
                    name: 'sorted in ascending order',
                    value: 1,
                },
                {
                    name: 'sorted in descending order', 
                    value: 2,
                },
            ],
        };
    }

    categorize(category) {
        const items = this.props.items;
        const newItems = []

        for (let item of items) {
            if (item.type === category) {
                newItems.push(item);
            }
        }

        let previousItemsLength = 0;
        for (let [type, items] of Object.entries(this.state.itemByTypes)) {
            if (CATEGORY_INDEX[type] < CATEGORY_INDEX[category]) {
                previousItemsLength += Math.ceil(items.length / (MAX_ROW * MAX_COL)) * (MAX_ROW * MAX_COL)
            }
        }
        
        this.setState({
            items: newItems,
            previousItemsLength,
            categoryIndex: CATEGORY_INDEX[category],
            page: 0,
        }, () => {
            this.sort(1);
        })
    }

    sort(type) {
        let items = [...this.state.items];
        let tempItems = [];
        type = parseInt(type);
        
        if (this.state.orderStatus === 2 && type === 1) {
            for(let index = items.length ; index>0; index--){
                tempItems.push(items[index-1]);
            }
            items = tempItems;
        } else if (this.state.orderStatus === 1 && type === 2) {
            for(let index = items.length ; index>0; index--){
                tempItems.push(items[index-1]);
            }
            items = tempItems;
        }

        console.log(items);

        this.setState({
            items,
            orderStatus: type,
        })
    }

    animateContent() {
        this.setState({
            menuButtonState: this.state.menuButtonIsClicked ? 'idle' : 'clicked',
            menuButtonIsClicked: !this.state.menuButtonIsClicked,
        });
    }

    componentWillMount() {
        this.categorize('hair');
    }

    changePage(goNext) {
        if (goNext) {
            this.setState({
                page: this.state.page + 1,
            });
        } else {
            this.setState({
                page: this.state.page - 1,
            });
        }
    }

    showItems() {
        const items = [];
        for (let row = 0; row < MAX_ROW; row++) {
            const rowList = []
            for (let col = 0; col < MAX_COL; col++) {
                const index = this.state.page * MAX_ROW * MAX_COL + row * MAX_COL + col; 
                const key = this.state.previousItemsLength + index;
                if (!this.state.items[index]) {
                    rowList.push(
                        <Item 
                            key={key}
                            id={index}
                            src='NONE'
                        />
                    );
                } else {
                    rowList.push(
                        <Item 
                            key={key}
                            id={index}
                            name={this.state.items[index].name}
                            type={this.state.items[index].type}
                            gender={this.props.isMale? 'm': 'f'}
                            src={this.state.items[index].url} 
                            do={() => this.props.setCharacterItem(
                                        this.state.items[index].type,
                                        this.state.items[index].id, 
                                        this.state.items[index].url)
                                }
                        />
                    );
                }
            }

            items.push(
                <div key={row} className='l-flex--row-nowrap l-flex--center-center'>
                    {rowList}
                </div>
            );
        }
        return items;
    }

    showPagination() {
        let leftArrow = <img key={1} id='c-item-menu__arrow--left' className='l-item-menu__arrow' src='/icons/empty-arrow.svg' alt='empty-arrow' />;
        let rightArrow = <img key={2} id='c-item-menu__arrow--right' className='l-item-menu__arrow' src='/icons/empty-arrow.svg' alt='empty-arrow' />;

        if (this.state.page > 0) {
            leftArrow = <img key={1} id='c-item-menu__arrow--left' className='l-item-menu__arrow' onClick={() => this.changePage(false)} src='/icons/filled-arrow.svg' alt='filled-arrow' />;
        }
        if (this.state.items[(this.state.page + 1) * MAX_ROW * MAX_COL + 1] !== undefined) {
            rightArrow = <img key={2} id='c-item-menu__arrow--right' className='l-item-menu__arrow' onClick={() => this.changePage(true)} src='/icons/filled-arrow.svg' alt='filled-arrow' />;
        }

        let pagination = [leftArrow, rightArrow];

        return (pagination);
    }
    

    render() {
        return (
            <div id='l-item-menu' className='l-flex--column-nowrap l-flex-center-center' data-display={this.state.displayState} data-start={this.state.menuButtonStart}>
                <button id='l-item-menu__button' className='h-animation-config' data-state={this.state.menuButtonState} onClick={() => this.animateContent()}>
                    <div id='l-item-menu__line-art'>
                        <div id='c-item-menu__line-art--0' className='c-item-menu__line-art h-animation-config'></div>
                        <div id='c-item-menu__line-art--1' className='c-item-menu__line-art h-animation-config'></div>
                        <div id='c-item-menu__line-art--2' className='c-item-menu__line-art h-animation-config'></div>
                    </div>
                </button>
                <div id='l-item-menu__content' className='h-animation-config'>
                    <div id='l-item-menu__head' className='l-flex--row-nowrap l-flex--center-center'>
                        <Dropdown src='/icons/category.svg' color={'black'} list={this.state.categoryList} sendValue={(value) => this.categorize(value)} />
                        <Dropdown src='/icons/sort.svg' color={'black'} list={this.state.sorterList} sendValue={(value) => this.sort(value)} />
                    </div>
                    <div id='l-item-menu__body'>
                        {this.showItems()}
                    </div>
                    <div id='l-item-menu__pagination' className='l-flex--row-nowrap l-flex--center-center'>
                        {this.showPagination()}
                    </div>
                </div>
                {/* <div id='l-item-menu__space'></div> */}
            </div>
        );
    }
}

export default ItemMenu;