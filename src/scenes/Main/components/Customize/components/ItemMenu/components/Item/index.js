import React, { Component } from 'react';
import './style.scss';

class Item extends Component {
    constructor(props) {
        super(props);

        this.parent = React.createRef();
        this.label = React.createRef();
        this.moveLabel = this.moveLabel.bind(this);
    }

    moveLabel(e) {
        const con = this.parent.current.getBoundingClientRect();
        let x = e.clientX - con.left;
        let y = e.clientY - con.top;

        let offsetX = 15;
        let offsetY = -10;

        x = x + offsetX;
        y = y + offsetY;

        this.label.current.style.left = `${x}px`;
        this.label.current.style.top = `${y}px`;
    }

    removeTransparentSpace(url) {
        return url
        
        // let img = new Image();
        // img.src = url;
        // img.crossOrigin = "Anonymous";
        // img.onload = () => {
        //     let c = document.createElement('canvas');
        //     let context = c.getContext('2d');
    
        //     context.drawImage(img, 0, 0, img.width, img.height);
    
        //     var ctx = c.getContext('2d'),
        //         copy = document.createElement('canvas').getContext('2d'),
        //         pixels = ctx.getImageData(0, 0, c.width, c.height),
        //         l = pixels.data.length,
        //         i,
        //         bound = {
        //             top: null,
        //             left: null,
        //             right: null,
        //             bottom: null
        //         },
        //         x, y;
            
        //     // Iterate over every pixel to find the highest
        //     // and where it ends on every axis ()
        //     for (i = 0; i < l; i += 4) {
        //         if (pixels.data[i + 3] !== 0) {
        //             x = (i / 4) % c.width;
        //             y = ~~((i / 4) / c.width);
        
        //             if (bound.top === null) {
        //                 bound.top = y;
        //             }
        
        //             if (bound.left === null) {
        //                 bound.left = x;
        //             } else if (x < bound.left) {
        //                 bound.left = x;
        //             }
        
        //             if (bound.right === null) {
        //                 bound.right = x;
        //             } else if (bound.right < x) {
        //                 bound.right = x;
        //             }
        
        //             if (bound.bottom === null) {
        //                 bound.bottom = y;
        //             } else if (bound.bottom < y) {
        //                 bound.bottom = y;
        //             }
        //         }
        //     }
            
        //     // Calculate the height and width of the content
        //     var trimHeight = bound.bottom - bound.top,
        //         trimWidth = bound.right - bound.left,
        //         trimmed = ctx.getImageData(bound.left, bound.top, trimWidth, trimHeight);
        
        //     copy.canvas.width = trimWidth;
        //     copy.canvas.height = trimHeight;
        //     copy.putImageData(trimmed, 0, 0);
        
        //     // Return trimmed canvas
        //     return copy.canvas.toDataURL;
        // }
    }

    showItem() {
        if (this.props.src === 'NONE') {
            return (
                <div id='l-item--empty' className='l-item l-flex--row-nowrap l-flex--center-center h-animation-config'>
                    <div id='c-line-art--0' className='c-line-art h-animation-config'></div>
                    <div id='c-line-art--1' className='c-line-art h-animation-config'></div>
                </div>
            );
        } else {
            return (
                <div id='l-item--not-empty' className='l-item l-flex--row-nowrap l-flex--center-center h-animation-config' onClick={this.props.do} onMouseMove={this.moveLabel} ref={this.parent}>
                    <div id='c-label' className='h-animation-config' data-state='mouse-leave' ref={this.label}>{this.props.name}</div>
                    <div
                        style={{backgroundImage: `url(${this.removeTransparentSpace(this.props.src)})`}} 
                        className='c-item__img' 
                        data-type={this.props.type} 
                        data-gender={this.props.gender} 
                        alt={this.props.type}>
                    </div>
                </div>
            );
        }
    }

    render() {
        return (
            this.showItem()
        );
    }
}

export default Item;