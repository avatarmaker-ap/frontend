import React, { Component } from 'react';
import './style.scss';

import Circle from 'components/Circle';

class Avatar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            displayState: this.props.display ? 'appear' : 'hide',
            gender: this.props.isMale? 'male': 'female',
            items: this.props.items,
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props !== nextProps) {
            this.setState({
                items: nextProps.items,
            });
        }
    }

    showItems() {
        const items = [];
        let index = 0;
        for (let item of Object.keys(this.state.items)) {
            if (this.state.items[item].url === undefined) {
                items.push(
                    <div key={index++} id={`l-avatar__${item}`} className='l-avatar__item'>
                    </div>
                );
            } else {
                items.push(
                    <div key={index++} id={`l-avatar__${item}`} className='l-avatar__item'>
                        <img alt={item} src={this.state.items[item].url} />
                    </div>
                );
            }
        }

        return items;
    }

    render() {
        return (
            <div id='l-avatar' data-state={this.state.displayState} >
                <div id='l-avatar__figure' data-gender={this.state.gender} >
                    <img src={`/imgs/${this.state.gender}-figure.svg`} className='c-avatar__figure' alt={`${this.state.gender}-character`}/>
                    <div id='l-avatar__items' >
                        {this.showItems()}
                    </div>
                </div>
                <div id='l-avatar__circle'>
                    <Circle />
                </div>
            </div>
        );
    }
}

export default Avatar;