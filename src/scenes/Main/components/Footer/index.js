import React from 'react';
import './style.scss';

const Footer = () => {
    return (
        <footer id='l-footer' className='l-flex--row-nowrap'>
            <div id='l-footer__left-footer' className='l-flex--row-nowrap'>
                <label id='c-footer__copyright'>
                    &copy; copyright 2019. all rights reserved. 
                </label>
            </div>
            <div id='l-footer__right-footer' className='l-flex--row-nowrap'>
                <label id='c-footer__developer-title'>
                    developers
                </label>
                <div id='c-footer__developer-names'>
                    arif teguh - dave nathanael - michael christopher m. - nandhika prayoga - sayid abyan r. s.
                </div>
            </div>
        </footer>
    );
}

export default Footer;