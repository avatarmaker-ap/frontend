import React, {Component} from 'react';
import './style.scss'

import Circle from 'components/Circle';
import Title from 'components/Title';

class GenderOption extends Component {
    constructor(props) {
        super(props);
        this.state = {
            displayState: this.props.display ? 'appear' : 'hide',
            maleCircleState: 'mouse-leave',
            femaleCircleState: 'mouse-leave',
        };
        this.moveLabel = this.moveLabel.bind(this);
        this.setCircleState = this.setCircleState.bind(this);
        
        this.parent = React.createRef();
        this.label = React.createRef();
    }

    setCircleState(isMale, doesMouseEnter) {
        if (isMale) {
            this.setState({
                maleCircleState: doesMouseEnter? 'mouse-enter': 'mouse-leave',
            });
        } else {
            this.setState({
                femaleCircleState: doesMouseEnter? 'mouse-enter': 'mouse-leave',
            });
        }
        
        this.changeLabel(isMale, doesMouseEnter);
    }

    changeLabel(isMale, appear) {
        if (appear) {
            this.label.current.setAttribute('data-state', 'mouse-enter');
        } else {
            this.label.current.setAttribute('data-state', 'mouse-leave');
        }

        if (isMale) {
            this.label.current.textContent = "male";
        } else {
            this.label.current.textContent = "female";
        }
    }

    moveLabel(e) {
        const con = this.parent.current.getBoundingClientRect();
        let x = e.clientX - con.left;
        let y = e.clientY - con.top;

        const offsetX = 15;
        const offsetY = -10;

        x = x + offsetX;
        y = y + offsetY;

        this.label.current.style.left = `${x}px`;
        this.label.current.style.top = `${y}px`;
    }

    moveToCustomize(isMale) {
        this.props.setScene(1, isMale);
    }

    render() {
        return (
            <section id='l-gender-option' className='l-section l-flex--column-nowrap l-flex--center-center' data-display={this.state.displayState} ref={this.parent} onMouseMove={(e)=> this.moveLabel(e)}>
                <Title name={'gender option'} type={0}/>
                <div id='c-label' className='h-animation-config' data-state='mouse-leave' ref={this.label}></div>
                <div id='l-gender-option__figure' className='l-flex--row-nowrap l-flex--center-center'>
                    <div id='l-figure__male' className='l-figure' onMouseEnter={() => this.setCircleState(true, true)} onMouseLeave={() => this.setCircleState(true, false)}>
                        <img src='/imgs/male-figure.svg' id='c-figure__male' className='l-figure__person h-animation-config' onClick={this.moveToCustomize.bind(this, true)} alt='male-figure' />
                        <div id='l-figure__circle' className='h-animation-config' data-state={this.state.maleCircleState} >
                            <Circle />
                        </div>
                    </div>
                    <div id='l-figure__female' className='l-figure' onMouseEnter={() => this.setCircleState(false, true)} onMouseLeave={() => this.setCircleState(false, false)}>
                        <img src='/imgs/female-figure.svg' id='c-figure__female' className='l-figure__person h-animation-config' onClick={this.moveToCustomize.bind(this, false)} alt='female-figure' />
                        <div id='l-figure__circle' className='h-animation-config' data-state={this.state.femaleCircleState} >
                            <Circle />
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default GenderOption;