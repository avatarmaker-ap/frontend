import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './style.scss';

class Loading extends Component {
    constructor(props) {
        super(props);
        this.state = {
            message: this.props.message,
            display: this.props.display,
            messageStyle: {
                position: `absolute`,
                left: this.props.config.x,
                top: this.props.config.y,
                transform: `translate(-50%, -50%)`,
                display: !this.props.display ? 'none': '',
            }
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props !== nextProps) {
            this.setState({
                message: nextProps.message,
                display: nextProps.display,
            });
        }
    }

    render() {
        return (
            <section style={this.state.messageStyle}>
                <div className='l-flex--column-nowrap l-flex--center-center'>
                    <label id='c-loading__message'>{this.state.message}</label>
                </div>
            </section>
        );
    }
}

Loading.propTypes = {
    setLoading: PropTypes.func
}

export default Loading;