import React, {Component} from 'react';

import Background from 'components/Background';
import Popup from 'components/Popup';
import Logo from 'components/Logo';
import Loading from './components/Loading';
import GenderOption from './components/GenderOption';
import Customize from './components/Customize';
import Footer from './components/Footer';

const LOADING_LOGO_CONFIG = {
    pos: `CENTER`,
    size: `LARGE`
};

const NOT_LOADING_LOGO_CONFIG = {
    pos: `LEFT_TOP`,
    size: `MEDIUM`,
};

const LOADING_STYLE = {
    x: `50%`,
    y: `calc(50% + 70px)`,
};

class Main extends Component {
    constructor(props) {
        super(props);
        this.setScene = this.setScene.bind(this);
        this.setPopup = this.setPopup.bind(this);
        this.setLoading = this.setLoading.bind(this);

        this.state = {
            logoConfig: LOADING_LOGO_CONFIG,
            loading: {
                isLoading: true,
                message: "welcome",
            },
            popup: {
                isActive: false,
                title: "",
                message: "",
            },
            scene: <div></div>,
        };

        setTimeout(() => {
            this.setScene(0);
            this.setLoading(false, "");
        }, 1500);
    }

    setScene(scenePage, isMale) {
        let scene;
        switch(scenePage) {
            case 0: {
                scene = (
                    <GenderOption 
                        display={true} 
                        setScene={(page, isMale) => this.setScene(page, isMale)}
                        setLoading={(isLoading, message) => this.setLoading(isLoading, message)}
                        showMessage={(title, message) => this.setPopup(true, title, message)} 
                    />
                );
                break;
            } case 1: {
                scene = (
                    <Customize 
                        display={true} 
                        isMale={isMale} 
                        setScene={(page) => this.setScene(page)}
                        setLoading={(isLoading, message) => this.setLoading(isLoading, message)}
                        showMessage={(title, message) => this.setPopup(true, title, message)}     
                    />
                );
            } 
        }

        this.setState({
            scene,
        })
    }
    
    setLoading(isLoading, message) {
        this.setState({
            loading: {
                isLoading,
                message,
            },
            logoConfig: isLoading ? LOADING_LOGO_CONFIG : NOT_LOADING_LOGO_CONFIG,
        });
    }

    setPopup(isActive, title, message) {
        this.setState({
            popup: {
                isActive,
                title,
                message,
            }
        });
    }

    render() {
        return (
            <section>
                <Popup
                    title={this.state.popup.title} 
                    isActive={this.state.popup.isActive} 
                    message={this.state.popup.message}
                    setPopup={(isActive, title, message) => this.setPopup(isActive, title, message)} 
                />
                <Background type={`MAIN`} display={true} />
                <Background type={`LOADING`} display={this.state.loading.isLoading} />
                <Loading  
                    config={LOADING_STYLE} 
                    display={this.state.loading.isLoading} 
                    message={this.state.loading.message}
                />
                {this.state.scene}
                <Logo config={this.state.logoConfig} />
                <Footer />
            </section>
        );
    }
}

export default Main;