import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import './style.scss';

import NotFound from 'scenes/NotFound';
import Main from 'scenes/Main';
class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Router>
          <Switch>
            <Route exact path='/' component={Main} />
            <Router path='' component={NotFound} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;